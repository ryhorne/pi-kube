# pi-kube

## WORK IN PROGRESS

My home setup for my Raspberry Pi 3b+ Kubernetes cluster. Be warned, some **hardcoded** IP address values may exist in playbooks.

This utilizes Ansible playbooks to work with Kubernetes. It appears to be a good combination, but there's probably a better way to do this.

## Services and other tidbits:

* flannel network (weave was overkill for the pi's. I don't need TLS/SSL communication)
* metallb + traefik for LoadBalancer + Ingress (maybe overkill)
    * specifying 192.168.1.245 as the traefik service
    * the goal is to terminate TLS at the Ingress so that the services don't have to work in TLS all the time
* DNS is handled via PfSense (192.168.1.245 == pi-kube.local.ryan, and additionally, anything.pi-kube routes to 192.168.1.245):

```
server:
local-zone: "pi-kube" redirect
local-data: "pi-kube 86400 IN A 192.168.1.245"
```

## Raspberry Pi Limitations:

* There's some specific performance limitations that I can't seem to figure out with the networking layer
* I can't use the following cool things, because they are only build for amd64 and not armv7l:
    * Any Envoy Proxy based thing, such as Gloo, Ambassador, etc
    * Rook (probably CephFS too)


![pi-kube without cables](pi1.jpg)
![pi-kube with cables](pi2.jpg)

