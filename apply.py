#!/usr/bin/env python

import subprocess
import os
import json

def verify_full_paths(paths):
    for p in paths:
        if not (os.path.isdir(p) or os.path.isfile(p)):
            raise ValueError("%s does not appear to be a valid path, or is a symbolic link" % (p))

    return paths


def get_full_paths(paths):
    full_paths = []
    for p in paths:
        fp = os.path.abspath(p)
        if os.path.isdir(p):
            fp += "/"
        full_paths.append(fp)

    return verify_full_paths(full_paths)


def call_playbook(paths, kube_action):
    extravars = {
        'kube_action': kube_action,
        'paths': paths
    }
    cmd = "ansible-playbook kubectl_apply.yml -e '%s'" % json.dumps(extravars)
    print("running: %s" % (cmd))
    subprocess.call([cmd], shell=True)


def main(paths, kube_action="apply"):
    paths = get_full_paths(paths)
    call_playbook(paths, kube_action)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--create', dest='create', default=False, action="store_true", help="create instead of apply")
    parser.add_argument('--delete', dest='delete', default=False, action="store_true", help="delete instead of apply")
    parser.add_argument('paths', nargs='+', help='paths to kubectl apply')
    args = parser.parse_args()

    if args.create and args.delete:
        raise ValueError("can't utilize both 'create' and 'delete' actions")

    kube_action = "apply"
    if args.create:
        kube_action = "create"
    elif args.delete:
        kube_action = "delete"

    main(args.paths, kube_action)

