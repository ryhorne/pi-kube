#!/usr/bin/env python2

import os
import subprocess
import sys

class bcolors:  # Thanks stackoverflow!
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

with open('ansible.cfg') as f:
    lines = [x.strip() for x in f]

invfile = None
for line in lines:
    if 'inventory' in line:
        invfile = line.split('=')[-1].strip()

if not invfile:
    sys.stderr.write("No inventory line found in ansible.cfg! Are you sure you're in the right directory?\n")
    sys.exit(1)

with open(invfile) as f:
    lines = [x.strip() for x in f]

print(bcolors.WARNING)
print("! ! ! DANGEROUS ! ! !")
print(bcolors.ENDC)
print("I will destroy the entire kubernetes stack. I will poweroff all of the nodes.")
print("Please do not confirm if you have unsaved data or production traffic running!")
print(bcolors.FAIL)
for line in lines:
    if not line:
        continue
    if '[' not in line and ']' not in line:
        line_l = line.split()
        if '=' not in line_l[0] and '#' not in line_l[0]:
            print(line_l[0])
print(bcolors.ENDC)

print(bcolors.WARNING)
print("Proceed to destroy kubernetes stack and power off these hosts?")
print("! ! ! DANGEROUS ! ! !")
print(bcolors.ENDC)
userinput = raw_input('y/n ')
if userinput.lower().strip() in ['y', 'yes']:
    subprocess.call(['ansible-playbook destroy_cluster.yml --skip-tags rebootnow'], shell=True)
    subprocess.call(['ansible "all" -m shell -b -a "shutdown -t 1 \'Powered off via Ansible\'"'], shell=True)

